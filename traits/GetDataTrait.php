<?php


namespace app\traits;


use app\models\Employees;

trait GetDataTrait
{
    protected function getData(): array
    {
        return Employees::find()
            ->select(['e.*', 'd.name AS department', 'a.name AS access', 'a.level'])
            ->from('employees e')
            ->orderBy(['personnel_number' => 'SORT_DESK'])
            ->withDepartment()
            ->withAccess()
            ->exceptInactive()
            ->asArray()
            ->all();
    }
}