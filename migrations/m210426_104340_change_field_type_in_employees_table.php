<?php

use app\models\Employees;
use yii\db\Migration;

/**
 * Class m210426_104340_change_field_type_in_employees_table
 */
class m210426_104340_change_field_type_in_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(Employees::tableName(), "inactive", $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn(Employees::tableName(), "inactive", $this->smallInteger()->defaultValue(0));

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210426_104340_change_field_type_in_employees_table cannot be reverted.\n";

        return false;
    }
    */
}
