<?php

use app\models\Access;
use yii\db\Migration;

/**
 * Class m210426_122711_add_default_access
 */
class m210426_122711_add_default_access extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(Access::tableName(),[
            "level", "name"
        ], [
            ["1", "ADMINISTRATOR"],
            ["2", "SUPER 2"],
            ["3", "SUPER 3"],
            ["4", "OPERATOR"],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable(Access::tableName());
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210426_122711_add_default_access cannot be reverted.\n";

        return false;
    }
    */
}
