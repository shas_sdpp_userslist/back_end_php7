<?php

use app\models\Departments;
use yii\db\Migration;

/**
 * Class m210426_115124_add_default_departments
 */
class m210426_115124_add_default_departments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(Departments::tableName(),[
            'name',
        ], [
            ['ДДПП'],
            ['СР'],
            ['СГМ'],
            ['СДГПиБЗ'],
            ['Таможня'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable(Departments::tableName());

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210426_115124_add_default_departments cannot be reverted.\n";

        return false;
    }
    */
}
