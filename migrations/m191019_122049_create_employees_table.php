<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees}}`.
 */
class m191019_122049_create_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employees}}', [
            'id' => $this->primaryKey(),
            'personnel_number' => $this->integer()->notNull()->unique(),
            'name' => $this->string(45)->notNull(),
            'password' => $this->integer()->notNull(),
            'departments_id' => $this->integer()->notNull(),
            'access_id' => $this->integer()->notNull(),
            'inactive' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()
        ]);

        $this->createIndex('fk_employees_access_idx',
            'employees',
            'access_id');

        $this->addForeignKey('fk_employees_access',
            'employees',
            'access_id',
            'access',
            'id');

        $this->createIndex('fk_employees_departments_idx',
            'employees',
            'departments_id');

        $this->addForeignKey('fk_employees_departments',
            'employees',
            'departments_id',
            'departments',
            'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_employees_departments', 'employees');
        $this->dropIndex('fk_employees_departments_idx', 'employees');
        $this->dropForeignKey('fk_employees_access', 'employees');
        $this->dropIndex('fk_employees_access_idx', 'employees');
        $this->dropTable('{{%employees}}');
    }
}
