<?php

use yii\db\Migration;

/**
 * Class m191225_143827_add_field_to_departments
 */
class m191225_143827_add_field_to_departments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("departments", "full_name", $this->string(60));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("departments", "full_name");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191225_143827_add_field_to_departments cannot be reverted.\n";

        return false;
    }
    */
}
