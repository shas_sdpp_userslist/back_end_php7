FROM composer:latest AS composer

FROM registry.gitlab.com/shas_sdpp_userslist/php-host:dev AS base
WORKDIR /home/www/
COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY composer.* /home/www/
RUN composer install
COPY ./ /home/www/

FROM base AS develop
WORKDIR /home/www/
RUN chmod +x ./entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]

FROM registry.gitlab.com/shas_sdpp_userslist/php-host:latest AS production
WORKDIR /home/www/
COPY --from=base /home/www/ /home/www/
RUN chmod +x ./entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]