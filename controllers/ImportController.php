<?php


namespace app\controllers;


use app\models\Employees;
use app\models\ImportUsers;
use Yii;
use yii\rest\Controller;
use yii\web\UploadedFile;

class ImportController extends Controller
{
    protected function verbs(): array
    {
        return [
            'index' => ['POST']
        ];
    }

    /**
     * @return array
     */
    public function actionIndex(): array
    {
        $model = new ImportUsers();
        $model->attributes = Yii::$app->request->post();
        $model->uploadedFile = UploadedFile::getInstanceByName("excel_file");
        $data = $model->import();
        $result = [];
        $post = Yii::$app->request->post();
        Employees::deleteAllInDepartment($post['departments_id']);
        foreach ($data as $value) {
            $employee = Employees::findOne(['personnel_number' => $value['personnel_number']]);
            if (is_null($employee)) {
                $employee = new Employees();
                $employee->scenario = Employees::SCENARIO_CREATE;
            } else {
                $employee->scenario = Employees::SCENARIO_UPDATE;
            }
            $employee->attributes = array_merge(
                $post,
                $value,
                ["inactive" => Employees::EMPLOYEE_ACTIVE]
            );
            try {
                if ($employee->save()) {
                    $result[$employee->scenario][] = $employee->personnel_number;
                } else {
                    foreach ($employee->getErrors() as $e_key => $error) {
                        $result['no valid'][$e_key][] = $error;
                    }
                }
            } catch (\Throwable $e) {
                $result['undone'][$e->getCode()][] = $employee->personnel_number;
            }
        }
        return $result;
    }
}