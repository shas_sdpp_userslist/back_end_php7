<?php

namespace app\controllers;

use app\models\ExportToFile;
use app\models\HiScanRow;
use app\models\ServerHiScanRow;
use app\traits\GetDataTrait;

class ServerhitraxController extends ExportController
{
    use GetDataTrait;

    public $exportModel = ServerHiScanRow::class;

    public function actionIndex(): bool
    {
        $date = $this->getData();
        $rows = new ExportToFile($this->exportModel);
        $rows->save($date)
            ->send();
        return true;
    }
}