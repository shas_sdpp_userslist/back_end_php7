<?php


namespace app\controllers;


use app\models\TsScanRow;
use app\traits\GetDataTrait;

class TsscanController extends ExportController
{
    use GetDataTrait;

    public $exportModel = TsScanRow::class;
}