<?php


namespace app\controllers;


use app\models\Employees;
use yii\rest\ActiveController;

class UserController extends ActiveController
{
    /**
     * @var Employees
     */
    public $modelClass = Employees::class;

    public $createScenario = Employees::SCENARIO_CREATE;

}