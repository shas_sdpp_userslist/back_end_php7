<?php


namespace app\controllers;


use app\models\Departments;
use yii\rest\ActiveController;

class DepartmentController extends ActiveController
{
    public $modelClass = Departments::class;
}