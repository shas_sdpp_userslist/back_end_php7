<?php


namespace app\controllers;

use app\models\Export;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\rest\Controller;

abstract class ExportController extends Controller
{
    public $exportModel;

    protected function verbs(): array
    {
        return [
            'index' => ['GET'],
        ];
    }

    /**
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if ($this->exportModel === null) {
            throw new InvalidConfigException('The "modelClass" property must be set.');
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function actionIndex(): bool
    {
        $data = $this->getData();
        $rows = new Export($this->exportModel);
        $rows->save($data)
            ->send();
        return true;
    }

    abstract protected function getData();
}