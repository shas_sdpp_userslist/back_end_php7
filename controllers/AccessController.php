<?php


namespace app\controllers;


use app\models\Access;
use yii\rest\ActiveController;

class AccessController extends ActiveController
{
    public $modelClass = Access::class;
}