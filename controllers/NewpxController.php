<?php


namespace app\controllers;


use app\models\NewPXRow;
use app\traits\GetDataTrait;

class NewpxController extends ExportController
{
    use GetDataTrait;

    public $exportModel = NewPXRow::class;
}