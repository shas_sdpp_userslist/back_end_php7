<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => getenv("PDO_CONNECT"),
    'username' => getenv('PDO_USER'),
    'password' => getenv('PDO_PASSWORD'),
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
