<?php


namespace app\models;


use yii\web\UploadedFile;

/**
 * Class ImportUsers
 * @package app\models
 *
 * @property int $personnel_number
 * @property int $name
 * @property int $password
 * @property UploadedFile $uploadedFile
 *
 */
class ImportUsers extends Import
{
    public $personnel_number;
    public $name;
    public $password;

    public function rules(): array
    {
        return [
            [['personnel_number', 'name', 'password'], 'required'],
            [['personnel_number', 'name', 'password'], 'integer'],
            [['uploadedFile'], 'file']
        ];
    }

    public function attributes(): array
    {
        return [
            "personnel_number",
            "name",
            "password",
        ];
    }
}