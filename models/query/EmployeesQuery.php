<?php


namespace app\models\query;


use app\models\Employees;
use yii\db\ActiveQuery;

class EmployeesQuery extends ActiveQuery
{
    public function exceptInactive(): EmployeesQuery
    {
        return $this->andWhere(['=', 'inactive', Employees::EMPLOYEE_ACTIVE]);
    }

    public function inactiveOnly(): EmployeesQuery
    {
        return $this->andWhere(['=', 'inactive', Employees::EMPLOYEE_INACTIVE]);
    }

    public function withDepartment(): EmployeesQuery
    {
        return $this->leftJoin('departments d', 'd.id = e.departments_id');
    }

    public function withAccess(): EmployeesQuery
    {
        return $this->leftJoin('access a', 'a.id = e.access_id');
    }
}