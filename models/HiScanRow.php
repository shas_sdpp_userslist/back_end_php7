<?php


namespace app\models;


class HiScanRow extends Row
{
    public static $row_separator = PHP_EOL;
    protected $_field_separator = PHP_EOL;
    private $_personnel_number;
    private $_name;
    private $_password;
    private $_department;
    private $_access;
    private $_superuser_postfix;

    public static function getSuperuser(): ?HiScanRow
    {
        $model = new static();
        $model->_personnel_number = "004";
        $model->_name = "SERVICE";
        $model->_password = "94866";
        $model->_department = "HEIMANN SYSTEMS";
        $model->_access = "FACTORY";
        $model->_superuser_postfix = 'permanent';
        return $model;
    }

    /**
     * @inheritDoc
     */
    public static function getFileName()
    {
        return 'hiscan/passwd/home/hitrax/data/passwd';
    }

    public static function getArchiveName(): string
    {
        return 'passwd';
    }

    public function rules(): array
    {
        return [
            [['personnel_number', 'name', 'password', 'department', 'access'], 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'personnel_number',
            'name',
            'password',
            'department',
            'access',
            'faaid',
            'superuser_postfix'
        ];
    }

    /**
     * @return string
     */
    public function getPersonnel_number(): string
    {
        return $this->fieldToString($this->_personnel_number, "username");
    }

    /**
     * @param string $personnel_number
     */
    public function setPersonnel_number(string $personnel_number): void
    {
        $this->_personnel_number = $this->bringTo($personnel_number, self::LOGIN_MIN_LENGTH, self::LOGIN_MAX_LENGTH);
    }

    protected function fieldToString(string $value, string $key = ''): string
    {
        return $key . "=" . $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->fieldToString($this->_name, 'fullname');
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->_name = $this->transliteration::run($name);
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->fieldToString($this->_password, 'password');
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->_password = $this->bringTo($password, self::PASSWORD_MIN_LENGTH, self::PASSWORD_MAX_LENGTH);
    }

    /**
     * @return string
     */
    public function getDepartment(): string
    {
        return $this->fieldToString($this->_department, 'company');
    }

    /**
     * @param string $department
     */
    public function setDepartment(string $department): void
    {
        $this->_department = $this->transliteration::run($department);
    }

    /**
     * @return string
     */
    public function getAccess(): string
    {
        return $this->fieldToString($this->_access, 'operatorid');
    }

    /**
     * @param string $access
     */
    public function setAccess(string $access): void
    {
        $this->_access = $access;
    }

    /**
     * @return string
     */
    public function getFaaid(): string
    {
        return $this->fieldToString('NONE', 'faaid');
    }

    /**
     * @return mixed
     */
    public function getSuperuser_postfix()
    {
        return $this->_superuser_postfix;
    }
}