<?php


namespace app\models;


use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\Model;

abstract class Row extends Model
{
    protected const LOGIN_MIN_LENGTH = 5;
    protected const LOGIN_MAX_LENGTH = 9;

    public const PASSWORD_MIN_LENGTH = 4;
    public const PASSWORD_MAX_LENGTH = 9;
    public static $row_separator = PHP_EOL;
    /**
     * @var Transliteration
     */
    protected $transliteration;
    protected $_field_separator;

    /**
     * Row constructor.
     * @param array $config
     * @throws InvalidConfigException
     */
    public function __construct($config = [])
    {
        if (!isset($this->_field_separator)) {
            throw new InvalidConfigException('$_field_separator is undefined');
        }
        $this->transliteration = Transliteration::class;
        parent::__construct($config);
    }

    /**
     * @return mixed
     */
    abstract public static function getFileName();

    public static function getArchiveName(): string
    {
        return '';
    }

    public static function getSuperuser()
    {
        return null;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function toString(): string
    {
        if (!$this->validate()) {
            throw new Exception(
                "Validation exception for '" . join(PHP_EOL, $this->attributes) . "'"
            );
        }
        return join($this->_field_separator, $this->attributes);
    }

    protected function increaseTo(string $value, $len)
    {
        return str_pad($value, $len, "0", STR_PAD_LEFT);
    }

    protected function reduceTo(string $value, int $len): string
    {
        return substr($value, 0 - $len);
    }

    protected function bringTo(string $value, int $min_length, int $max_length): string
    {
         if (strlen($value) < $min_length) {
             return $this->increaseTo($value, $min_length);
         }
         if (strlen($value) > $max_length) {
             return $this->reduceTo($value, $max_length);
         }
         return $value;
    }
}