<?php


namespace app\models;


use moonland\phpexcel\Excel;
use yii\base\Model;
use yii\web\UploadedFile;

class Import extends Model
{
    /**
     * @var UploadedFile
     */
    public $uploadedFile;

    /**
     * @return \Generator
     */
    public function import(): \Generator
    {
        if (!$this->validate()) {
            yield $this->getErrors();
        }
        /** @var array $data */
        $data = Excel::import($this->uploadedFile->tempName, [
            "setFirstRecordAsKeys" => false,
        ]);
        foreach ($data as $val) {
            $user = [];
            foreach ($this->attributes as $key => $item) {
                $user[$key] = $val[chr(65 + $item)];
            }
            yield $user;
        }
    }

}