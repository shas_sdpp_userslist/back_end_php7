<?php


namespace app\models;


use Yii;
use yii\base\Exception;

class ExportToFile extends Export
{
    protected $_root;

    protected $_file;

    /**
     * ExportToFile constructor.
     * @param $row
     * @param array $config
     * @throws Exception
     */
    public function __construct($row, $config = [])
    {
        parent::__construct($row, $config);
        $this->_root = Yii::getAlias('@files') . '/';
        $this->_file = fopen($this->_root . $this->_row::getFileName(), 'wt');
        if ($this->_file === false) {
            throw new Exception('Can not create or open file ' . $this->_row::getFileName());
        }
    }

    /**
     * @throws Exception
     */
    public function send()
    {
        $len = fwrite($this->_file, $this->_data);
        fclose($this->_file);
        if ($len !== false) {
//            $file = Yii::$app->response->xSendFile('/files/' . $this->archived(), null, ['xHeader' => 'X-Accel-Redirect']);
            $file = Yii::$app->response->sendFile($this->_root . $this->archived(), $this->_row::getArchiveName());
            $file->send();
        } else {
            throw new Exception("No data to export");
        }
    }

    /**
     * @return string
     */
    private function archived(): string
    {
        `cd ./files
        tar -zcvf passwd ./hiscan/
        cd ..`;
        return $this->_row::getArchiveName();
    }

}