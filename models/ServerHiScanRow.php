<?php

namespace app\models;

class ServerHiScanRow extends HiScanRow
{
    public static function getFileName()
    {
        return '/home/hitrax/data/passwd';
    }

    public static function getArchiveName(): string
    {
        return 'uipasswd';
    }
}