<?php


namespace app\models;


use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\web\RangeNotSatisfiableHttpException;

class Export extends Model
{
    /**
     * @var Row
     */
    protected $_row;

    /**
     * @var string
     */
    protected $_data;

    public function __construct($row, $config = [])
    {
        $this->_row = $row;
        parent::__construct($config);
    }

    /**
     * @param array $data
     * @return $this
     * @throws Exception
     */
    public function save(array $data): Export
    {
        $rows = [];
        $export = $this->_row;
        $class = new $export();
        $superuser = $class::getSuperuser();
        foreach ($data as $value) {
            $class->attributes = $value;
            $rows[] = $class->toString();
        }
        if (!is_null($superuser)) {
            $rows[] = $superuser->toString();
        }
        $this->_data = join($export::$row_separator, $rows);
        return $this;
    }

    /**
     * @throws RangeNotSatisfiableHttpException
     */
    public function send()
    {
        $file = Yii::$app->response->sendContentAsFile($this->_data, $this->_row::getFileName());
        $file->send();
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->_data;
    }
}