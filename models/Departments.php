<?php


namespace app\models;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * Class Departments
 * @package app\models
 *
 * @property int $id
 * @property string $name
 * @property string $full_name
 *
 * @property Employees[] $users
 */
class Departments extends ActiveRecord
{
    public static function tableName(): string
    {
        return "departments";
    }

    public function rules(): array
    {
        return [
            ['name', 'required'],
            [['name', 'full_name'], 'string']
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUsers(): ActiveQuery
    {
        return $this->hasMany(Employees::class, ['id', 'departments_id']);
    }
}