<?php


namespace app\models;


class TsScanRow extends Row
{
    protected $_field_separator = ";";
    private $_surname;
    private $_name;
    private $_patronymic;
    private $_personnel_number;
    private $_password;
    private $_level;
    private $_zero_field;

    public static function getSuperuser(): ?TsScanRow
    {
        $model = new static();
        $model->_surname = "1";
        $model->_name = "1";
        $model->_patronymic = '1';
        $model->_personnel_number = '123456';
        $model->_password = "131313";
        $model->_zero_field = 0;
        $model->_level = "2";
        return $model;
    }

    /**
     * @return string
     */
    public static function getFileName(): string
    {
        return 'tsscan.txt';
    }

    public function rules(): array
    {
        return [
            [['name', 'personnel_number', 'password', 'level'], 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'surname',
            'name',
            'patronymic',
            'personnel_number',
            'password',
            'zeroField',
            'level'
        ];
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->transliteration::run($this->_surname);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->transliteration::run($this->_name);
    }

    /**
     * @param void $name
     */
    public function setName($name): void
    {
        $name = preg_replace("/\s{2,}/", " ", $name);
        $name = explode(' ', $name);
        $this->_surname = $name[0];
        $this->_name = $name[1] ?? '';
        $this->_patronymic = $name[2] ?? '';
    }

    /**
     * @return string
     */
    public function getPatronymic(): string
    {
        return $this->transliteration::run($this->_patronymic);
    }

    /**
     * @return string
     */
    public function getPersonnel_number(): string
    {
        return $this->bringTo($this->_personnel_number, self::LOGIN_MIN_LENGTH, self::LOGIN_MAX_LENGTH);
    }

    /**
     * @param string $personnel_number
     */
    public function setPersonnel_number(string $personnel_number): void
    {
        $this->_personnel_number = $personnel_number;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->bringTo($this->_password, self::PASSWORD_MIN_LENGTH, self::PASSWORD_MAX_LENGTH);
    }

    /**
     * @param void $password
     */
    public function setPassword($password): void
    {
        $this->_password = $password;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->_level;
    }

    /**
     * @param int $level
     */
    public function setLevel(int $level): void
    {
        $this->_level = ($level == 1) ? 2 : 1;
    }

    public function getZeroField(): int
    {
        return $this->_zero_field ?? 0;
    }
}