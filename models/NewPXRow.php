<?php


namespace app\models;


class NewPXRow extends Row
{
    protected $_field_separator = "\t";
    /**
     * @var string
     */
    private $_surname;
    /**
     * @var string
     */
    private $_name;
    /**
     * @var string
     */
    private $_patronymic;
    /**
     * @var string
     */
    private $_personnel_number;
    /**
     * @var string
     */
    private $_password;
    /**
     * @var string
     */
    private $_level;
    /**
     * @var string
     */
    private $_department;
    /**
     * @var string
     */
    private $_inactive;
    /**
     * @var string
     */
    private $_updated_at;

    /**
     * @inheritDoc
     */
    public static function getFileName()
    {
        return 'new.txt';
    }

    public function rules(): array
    {
        return [
            [['name', 'personnel_number', 'password', 'level', 'department', 'inactive', 'updated_at'], 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'name',
            'patronymic',
            'surname',
            'personnel_number',
            'password',
            'department',
            'level',
            'inactive',
            'updated_at'
        ];
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->transliteration::run($this->_surname);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->transliteration::run($this->_name);
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $name = preg_replace("/\./", '. ', $name);
        $name = preg_replace("/\s{2,}/", " ", $name);
        $name = explode(' ', $name);
        $this->_surname = $name[0];
        $this->_name = $name[1] ?? '';
        $this->_patronymic = $name[2] ?? '';
    }

    /**
     * @return string
     */
    public function getPatronymic(): string
    {
        return $this->transliteration::run($this->_patronymic);
    }

    /**
     * @return string
     */
    public function getpersonnel_number(): string
    {
        return $this->bringTo($this->_personnel_number, self::LOGIN_MIN_LENGTH, self::LOGIN_MAX_LENGTH);
    }

    /**
     * @param string $personnel_number
     */
    public function setpersonnel_number(string $personnel_number): void
    {
        $this->_personnel_number = $personnel_number;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->bringTo($this->_password, self::PASSWORD_MIN_LENGTH, self::PASSWORD_MAX_LENGTH);
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->_password = $password;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->_level;
    }

    /**
     * @param string $level
     */
    public function setLevel(string $level): void
    {
        $this->_level = $level;
    }

    /**
     * @return string
     */
    public function getUpdated_at(): string
    {
        return date("d-M-y", (int)$this->_updated_at);
    }

    /**
     * @param string $updated_at
     */
    public function setUpdated_at(string $updated_at): void
    {
        $this->_updated_at = $updated_at;
    }

    /**
     * @return string
     */
    public function getNull(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getDepartment(): string
    {
        return $this->transliteration::run($this->_department);
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department): void
    {
        $this->_department = $department;
    }

    /**
     * @return string
     */
    public function getInactive(): string
    {
        return $this->_inactive;
    }

    /**
     * @param mixed $inactive
     */
    public function setInactive($inactive): void
    {
        $this->_inactive = $inactive;
    }
}