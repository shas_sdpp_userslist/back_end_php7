<?php


namespace app\models;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Access
 * @package app\models
 *
 * @property int $id
 * @property int $level
 * @property string $name
 *
 * @property Employees[] $users
 */
class Access extends ActiveRecord
{
    public static function tableName(): string
    {
        return "access";
    }

    public function rules(): array
    {
        return [
            [['level', 'name'], 'required'],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUsers(): ActiveQuery
    {
        return $this->hasMany(Employees::class, ['id', 'access_id']);
    }
}