<?php

namespace app\models;

use app\models\query\EmployeesQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * Class Employees
 * @package app\models
 *
 * @property int $id
 * @property int $personnel_number
 * @property string $name
 * @property int $password
 * @property int $access_id
 * @property int $departments_id
 * @property bool $inactive
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Access $access
 * @property Departments $departments
 */
class Employees extends ActiveRecord
{
    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';
    public const SCENARIO_DELETE = 'delete';

    const EMPLOYEE_INACTIVE = true;
    const EMPLOYEE_ACTIVE = false;

    public static function tableName(): string
    {
        return 'employees';
    }

    public static function find()
    {
        return Yii::createObject(EmployeesQuery::class, [get_called_class()]);
    }

    public function behaviors(): array
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::class
            ],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'inactive' => self::EMPLOYEE_INACTIVE
                ],
//                'replaceRegularDelete' => true Не срабатывает переопределение
            ]
        ];
    }

    public function scenarios(): array
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['personnel_number', 'name', 'password', 'departments_id', 'access_id'];
        $scenarios[self::SCENARIO_DELETE] = [];
        $scenarios[self::SCENARIO_UPDATE] = ['personnel_number', 'name', 'password', 'departments_id', 'access_id', 'inactive'];
        return $scenarios;
    }

    public function rules(): array
    {
        return [
            [['name', 'password', 'departments_id', 'access_id'], 'required', 'on' => self::SCENARIO_CREATE],
            [['name'], 'trim'],
            [['personnel_number'], 'required'],
            ['personnel_number', 'unique'],
            [['name'], 'string'],
            [['personnel_number', 'password', 'departments_id', 'access_id'], 'integer'],
            ['inactive', 'boolean'],
            ['inactive', 'default', 'value' => self::EMPLOYEE_ACTIVE],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAccess(): ActiveQuery
    {
        return $this->hasOne(Access::class, ['id' => 'access_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDepartments(): ActiveQuery
    {
        return $this->hasOne(Departments::class, ['id' => 'departments_id']);
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Имя сотрудника',
            'personnel_number' => 'Табельный №',
            'password' => 'Пароль',
            'inactive' => 'Блокировка',
            'departments_id' => 'ID подразделения',
            'access_id' => 'ID уровня допуска',
            'created_at' => 'Добавлен',
            'updated_at' => 'Отредактирован'
        ];
    }

    public function delete()
    {
        $this->softDelete();
    }

    public static function deleteAllInDepartment(int $department_ID): int
    {
        return static::updateAll(['inactive' => static::EMPLOYEE_INACTIVE, 'updated_at' => time()], ['and', ['=', 'departments_id', $department_ID], ['=', 'inactive', static::EMPLOYEE_ACTIVE]]);
    }
}
